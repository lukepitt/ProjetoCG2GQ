#include "MeuPainelOpenGL.h"
#include "GL/glu.h"
MeuPainelOpenGL::MeuPainelOpenGL(QWidget *parent) :
    QGLWidget(parent)
{
    setFormat(QGL::DoubleBuffer | QGL::DepthBuffer);
    setFocusPolicy(Qt::StrongFocus);


    iniX = 1;
    iniY = 1;
    iniZ = 1;

    x = iniX;
    y = iniY;
    z = iniZ;

    rotacaoX = 0;
    rotacaoY = 0;
    rotacaoZ = 0;

    translacaoX = 0;
    translacaoY = 0;
    translacaoZ = 0;

    scaleX = 100;
    scaleY = 100;
    scaleZ = 100;
}

void MeuPainelOpenGL::setEscalaX(int valor)
{
    scaleX = valor;
}

void MeuPainelOpenGL::setEscalaY(int valor)
{
    scaleY = valor;
}

void MeuPainelOpenGL::setEscalaZ(int valor)
{
    scaleZ = valor;
}

void MeuPainelOpenGL::setRotacaoX(int valor)
{
    rotacaoX = valor;
}

void MeuPainelOpenGL::setRotacaoY(int valor)
{
    rotacaoY = valor;
}

void MeuPainelOpenGL::setRotacaoZ(int valor)
{
    rotacaoZ = valor;
}

void MeuPainelOpenGL::setTranslacaoX(int valor)
{
    translacaoX = valor;
}

void MeuPainelOpenGL::setTranslacaoY(int valor)
{
    translacaoY = valor;
}

void MeuPainelOpenGL::setTranslacaoZ(int valor)
{
    translacaoZ = valor;
}

void MeuPainelOpenGL::animateObj()
{
    int i;
    int trslMaxX =  translacaoX, trslMaxY = translacaoY, trslMaxZ = translacaoZ;
    int rotMaxX =  rotacaoX,rotMaxY =  rotacaoY,rotMaxZ =  rotacaoZ;
    int sclMaxX = scaleX, scaleXold = scaleX;
    int waitTrs = 10, waitRt = 30,waitScls = 40;

    if(translacaoX != 0 || translacaoY != 0 || translacaoZ != 0 || rotacaoX != 0 || rotacaoY != 0 || rotacaoZ !=0  || scaleX != 100) {

        if(rotacaoX !=0){
            for(i=0;i<rotMaxX;i++){
                rotacaoX = i;
                glLoadIdentity();
                gluLookAt(0, 10, 20,0, 0, 0, 0, 1, 0);
                updateGL();
                Sleep(waitRt);
            }
        }

        if(rotacaoY !=0){
            for(i=0;i<rotMaxY;i++){
                rotacaoY = i;
                glLoadIdentity();
                gluLookAt(0, 10, 20,0, 0, 0, 0, 1, 0);
                updateGL();
                Sleep(waitRt);
            }
        }

        if(rotacaoZ !=0){
            for(i=0;i<rotMaxZ;i++){
                rotacaoZ = i;
                glLoadIdentity();
                gluLookAt(0, 10, 20,0, 0, 0, 0, 1, 0);
                updateGL();
                Sleep(waitRt);
            }
        }
        if(translacaoX !=0){
            for(i=0;i<trslMaxX;i++){
                translacaoX = i;
                glLoadIdentity();
                gluLookAt(0, 10, 20,0, 0, 0, 0, 1, 0);
                updateGL();
                Sleep(waitTrs);
            }
        }

        if(translacaoY !=0){
            for(i=0;i<trslMaxY;i++){
                translacaoY = i;
                glLoadIdentity();
                gluLookAt(0, 10, 20,0, 0, 0, 0, 1, 0);
                updateGL();
                Sleep(waitTrs);
            }
        }

    if(translacaoZ !=0){
        for(i=0;i<trslMaxZ;i++){
            translacaoZ = i;
            glLoadIdentity();
            gluLookAt(0, 10, 20,0, 0, 0, 0, 1, 0);
            updateGL();
            Sleep(waitTrs);
        }
    }

    /*if(scaleX < 100) {
            for(i=100;i>sclMaxX;i--){
                scaleX = i;
                glLoadIdentity();
                gluLookAt(0, 10, 20,0, 0, 0, 0, 1, 0);
                updateGL();
                Sleep(waitScls);
            }
        }*/
 }
    else{
        glLoadIdentity();
        gluLookAt(0, 10, 20,0, 0, 0, 0, 1, 0);
        updateGL();
    }

}

void MeuPainelOpenGL::initializeGL()
{
    qglClearColor(Qt::white);
    glClearDepth(1.0f);
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);
    glHint(GL_LINE_SMOOTH_HINT, GL_SMOOTH);
}

void MeuPainelOpenGL::resizeGL(int w, int h)
{

    if(w>h){
        //caso a largura seja maior, o x deve começar na metade da difereça entre a largura e a altura.
        glViewport((w-h)/2, 0, h, h);
    }else{
        //caso a altura seja maior, o y deve começar na metade da difereça entre a altura e a largura  .
        glViewport(0, (h-w)/2, w, w);
    }

  // glViewport(0, 0, w, h);
   glMatrixMode(GL_PROJECTION);
   glLoadIdentity();
   gluPerspective(15,w/h,0.1f,100);

   glMatrixMode(GL_MODELVIEW);
   glLoadIdentity();
   gluLookAt(0, 10, 20,0, 0, 0, 0, 1, 0);
}

void MeuPainelOpenGL::resetModel() {

    x = iniX;
    y = iniY;
    z = iniZ;

    //glLoadIdentity();
    //gluLookAt(0, 10, 20,0, 0, 0, 0, 1, 0);
    updateGL();

}

void MeuPainelOpenGL::paintGL()
{
glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

glRotatef(rotacaoX,1,0,0);
glRotatef(rotacaoZ,0,0,1);
glRotatef(rotacaoY,0,1,0);

glTranslatef(translacaoX,0,0);
glTranslatef(0,translacaoY,0);
glTranslatef(0,0,translacaoZ);

x = x*(scaleX/100);
y = y*(scaleY/100);
z = z*(scaleZ/100);

criarCubo(x,y,z);

glFlush();

}

void MeuPainelOpenGL::criarCubo(int x,int y,int z) {


    glBegin(GL_QUADS);                // Begin drawing the color cube with 6 quads
          // Top face (y = 1.0f)
          // Define vertices in counter-clockwise (CCW) order with normal pointing out
          glColor3f(0.5f, 0.2f, 0.1f);     // Green
          glVertex3f( x, y, -z);
          glVertex3f(-x, y, -z);
          glVertex3f(-x, y,  z);
          glVertex3f( x, y,  z);

          // Bottom face (y = -1.0f)
          glColor3f(0.3f, 0.4f, 0.2f);     // Orange
          glVertex3f( x, -y,  z);
          glVertex3f(-x, -y,  z);
          glVertex3f(-x, -y, -z);
          glVertex3f( x, -y, -z);

          // Front face  (z = 1.0f)
          glColor3f(1.0f, 0.0f, 0.0f);     // Red
          glVertex3f( x,  y, z);
          glVertex3f(-x,  y, z);
          glVertex3f(-x, -y, z);
          glVertex3f( x, -y, z);

          // Back face (z = -1.0f)
          glColor3f(0.0f, 0.0f, 0.3f);     // Yellow
          glVertex3f( x, -y, -z);
          glVertex3f(-x, -y, -z);
          glVertex3f(-x,  y, -z);
          glVertex3f( x,  y, -z);

          // Left face (x = -1.0f)
          glColor3f(0.2f, 0.5f, 1.0f);     // Blue
          glVertex3f(-x,  y,  z);
          glVertex3f(-x,  y, -z);
          glVertex3f(-x, -y, -z);
          glVertex3f(-x, -y, z);

          // Right face (x = 1.0f)
          glColor3f(1.0f, 0.3f, 1.0f);     // Magenta
          glVertex3f(x,  y, -z);
          glVertex3f(x,  y,  z);
          glVertex3f(x, -y,  z);
          glVertex3f(x, -y, -z);
       glEnd();  // End of drawing color-cube


}
