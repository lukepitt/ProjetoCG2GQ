#ifndef MEUPAINELOPENGL_H
#define MEUPAINELOPENGL_H

#include <QWidget>
#include <QGLWidget>

class MeuPainelOpenGL : public QGLWidget
{
    Q_OBJECT
public:
    explicit MeuPainelOpenGL(QWidget *parent = 0);

private:
    float rotacaoX;
    float rotacaoY;
    float rotacaoZ;

    float iniX;
    float iniY;
    float iniZ;

    float x;
    float y;
    float z;

    float translacaoX;
    float translacaoY;
    float translacaoZ;

    float scaleX;
    float scaleY;
    float scaleZ;

public slots:
    void setRotacaoX(int valor);
    void setRotacaoY(int valor);
    void setRotacaoZ(int valor);
    void setTranslacaoX(int valor);
    void setTranslacaoY(int valor);
    void setTranslacaoZ(int valor);
    void resetModel();
    void setEscalaX(int valor);
    void setEscalaY(int valor);
    void setEscalaZ(int valor);
    void animateObj();
    void criarCubo(int x, int y, int z);

protected:

    void initializeGL();
    void resizeGL(int w , int h);
    void paintGL();


signals:

public slots:
};

#endif // MEUPAINELOPENGL_H
