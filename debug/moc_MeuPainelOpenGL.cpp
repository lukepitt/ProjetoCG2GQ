/****************************************************************************
** Meta object code from reading C++ file 'MeuPainelOpenGL.h'
**
** Created by: The Qt Meta Object Compiler version 63 (Qt 4.8.7)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "../MeuPainelOpenGL.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'MeuPainelOpenGL.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.7. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_MeuPainelOpenGL[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      12,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       0,       // signalCount

 // slots: signature, parameters, type, tag, flags
      23,   17,   16,   16, 0x0a,
      40,   17,   16,   16, 0x0a,
      57,   17,   16,   16, 0x0a,
      74,   17,   16,   16, 0x0a,
      94,   17,   16,   16, 0x0a,
     114,   17,   16,   16, 0x0a,
     134,   16,   16,   16, 0x0a,
     147,   17,   16,   16, 0x0a,
     163,   17,   16,   16, 0x0a,
     179,   17,   16,   16, 0x0a,
     195,   16,   16,   16, 0x0a,
     214,  208,   16,   16, 0x0a,

       0        // eod
};

static const char qt_meta_stringdata_MeuPainelOpenGL[] = {
    "MeuPainelOpenGL\0\0valor\0setRotacaoX(int)\0"
    "setRotacaoY(int)\0setRotacaoZ(int)\0"
    "setTranslacaoX(int)\0setTranslacaoY(int)\0"
    "setTranslacaoZ(int)\0resetModel()\0"
    "setEscalaX(int)\0setEscalaY(int)\0"
    "setEscalaZ(int)\0animateObj()\0x,y,z\0"
    "criarCubo(int,int,int)\0"
};

void MeuPainelOpenGL::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        MeuPainelOpenGL *_t = static_cast<MeuPainelOpenGL *>(_o);
        switch (_id) {
        case 0: _t->setRotacaoX((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 1: _t->setRotacaoY((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 2: _t->setRotacaoZ((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 3: _t->setTranslacaoX((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 4: _t->setTranslacaoY((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 5: _t->setTranslacaoZ((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->resetModel(); break;
        case 7: _t->setEscalaX((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 8: _t->setEscalaY((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 9: _t->setEscalaZ((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 10: _t->animateObj(); break;
        case 11: _t->criarCubo((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2])),(*reinterpret_cast< int(*)>(_a[3]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData MeuPainelOpenGL::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject MeuPainelOpenGL::staticMetaObject = {
    { &QGLWidget::staticMetaObject, qt_meta_stringdata_MeuPainelOpenGL,
      qt_meta_data_MeuPainelOpenGL, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &MeuPainelOpenGL::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *MeuPainelOpenGL::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *MeuPainelOpenGL::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_MeuPainelOpenGL))
        return static_cast<void*>(const_cast< MeuPainelOpenGL*>(this));
    return QGLWidget::qt_metacast(_clname);
}

int MeuPainelOpenGL::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QGLWidget::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 12)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 12;
    }
    return _id;
}
QT_END_MOC_NAMESPACE
