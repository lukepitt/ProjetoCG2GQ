/********************************************************************************
** Form generated from reading UI file 'JanelaPrincipal.ui'
**
** Created by: Qt User Interface Compiler version 4.8.7
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_JANELAPRINCIPAL_H
#define UI_JANELAPRINCIPAL_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QSpinBox>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>
#include "meupainelopengl.h"

QT_BEGIN_NAMESPACE

class Ui_JanelaPrincipal
{
public:
    QWidget *centralWidget;
    QHBoxLayout *horizontalLayout;
    MeuPainelOpenGL *widget;
    QGroupBox *groupBox;
    QVBoxLayout *verticalLayout;
    QGroupBox *groupBox_2;
    QVBoxLayout *verticalLayout_2;
    QGroupBox *groupBox_7;
    QHBoxLayout *horizontalLayout_2;
    QSpinBox *spinBox;
    QGroupBox *groupBox_4;
    QHBoxLayout *horizontalLayout_3;
    QSpinBox *spinBox_3;
    QGroupBox *groupBox_3;
    QHBoxLayout *horizontalLayout_4;
    QSpinBox *spinBox_2;
    QGroupBox *groupBox_5;
    QVBoxLayout *verticalLayout_3;
    QGroupBox *groupBox_8;
    QHBoxLayout *horizontalLayout_7;
    QSpinBox *spinBox_7;
    QSpinBox *spinBox_6;
    QSpinBox *spinBox_4;
    QGroupBox *groupBox_11;
    QVBoxLayout *verticalLayout_4;
    QGroupBox *groupBox_12;
    QHBoxLayout *horizontalLayout_10;
    QSpinBox *spinBox_10;
    QSpinBox *spinBox_9;
    QSpinBox *spinBox_8;
    QPushButton *pushButton_2;
    QPushButton *playRX_10;
    QPushButton *pushButton;
    QMenuBar *menuBar;
    QMenu *menu3D;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *JanelaPrincipal)
    {
        if (JanelaPrincipal->objectName().isEmpty())
            JanelaPrincipal->setObjectName(QString::fromUtf8("JanelaPrincipal"));
        JanelaPrincipal->resize(848, 588);
        centralWidget = new QWidget(JanelaPrincipal);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        horizontalLayout = new QHBoxLayout(centralWidget);
        horizontalLayout->setSpacing(6);
        horizontalLayout->setContentsMargins(11, 11, 11, 11);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        widget = new MeuPainelOpenGL(centralWidget);
        widget->setObjectName(QString::fromUtf8("widget"));
        widget->setEnabled(true);
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(1);
        sizePolicy.setHeightForWidth(widget->sizePolicy().hasHeightForWidth());
        widget->setSizePolicy(sizePolicy);
        widget->setMinimumSize(QSize(500, 500));
        widget->setMaximumSize(QSize(900, 500));

        horizontalLayout->addWidget(widget);

        groupBox = new QGroupBox(centralWidget);
        groupBox->setObjectName(QString::fromUtf8("groupBox"));
        QSizePolicy sizePolicy1(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(groupBox->sizePolicy().hasHeightForWidth());
        groupBox->setSizePolicy(sizePolicy1);
        verticalLayout = new QVBoxLayout(groupBox);
        verticalLayout->setSpacing(6);
        verticalLayout->setContentsMargins(11, 11, 11, 11);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        groupBox_2 = new QGroupBox(groupBox);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        verticalLayout_2 = new QVBoxLayout(groupBox_2);
        verticalLayout_2->setSpacing(6);
        verticalLayout_2->setContentsMargins(11, 11, 11, 11);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        groupBox_7 = new QGroupBox(groupBox_2);
        groupBox_7->setObjectName(QString::fromUtf8("groupBox_7"));
        horizontalLayout_2 = new QHBoxLayout(groupBox_7);
        horizontalLayout_2->setSpacing(6);
        horizontalLayout_2->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        spinBox = new QSpinBox(groupBox_7);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));
        spinBox->setMaximum(360);

        horizontalLayout_2->addWidget(spinBox);


        verticalLayout_2->addWidget(groupBox_7);

        groupBox_4 = new QGroupBox(groupBox_2);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        horizontalLayout_3 = new QHBoxLayout(groupBox_4);
        horizontalLayout_3->setSpacing(6);
        horizontalLayout_3->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        spinBox_3 = new QSpinBox(groupBox_4);
        spinBox_3->setObjectName(QString::fromUtf8("spinBox_3"));
        spinBox_3->setMaximum(360);

        horizontalLayout_3->addWidget(spinBox_3);


        verticalLayout_2->addWidget(groupBox_4);

        groupBox_3 = new QGroupBox(groupBox_2);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        horizontalLayout_4 = new QHBoxLayout(groupBox_3);
        horizontalLayout_4->setSpacing(6);
        horizontalLayout_4->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        spinBox_2 = new QSpinBox(groupBox_3);
        spinBox_2->setObjectName(QString::fromUtf8("spinBox_2"));
        spinBox_2->setMaximum(360);

        horizontalLayout_4->addWidget(spinBox_2);


        verticalLayout_2->addWidget(groupBox_3);


        verticalLayout->addWidget(groupBox_2);

        groupBox_5 = new QGroupBox(groupBox);
        groupBox_5->setObjectName(QString::fromUtf8("groupBox_5"));
        verticalLayout_3 = new QVBoxLayout(groupBox_5);
        verticalLayout_3->setSpacing(6);
        verticalLayout_3->setContentsMargins(11, 11, 11, 11);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        groupBox_8 = new QGroupBox(groupBox_5);
        groupBox_8->setObjectName(QString::fromUtf8("groupBox_8"));
        horizontalLayout_7 = new QHBoxLayout(groupBox_8);
        horizontalLayout_7->setSpacing(6);
        horizontalLayout_7->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        spinBox_7 = new QSpinBox(groupBox_8);
        spinBox_7->setObjectName(QString::fromUtf8("spinBox_7"));
        spinBox_7->setMinimum(-5);
        spinBox_7->setMaximum(5);
        spinBox_7->setValue(0);

        horizontalLayout_7->addWidget(spinBox_7);

        spinBox_6 = new QSpinBox(groupBox_8);
        spinBox_6->setObjectName(QString::fromUtf8("spinBox_6"));
        spinBox_6->setMinimum(-5);
        spinBox_6->setMaximum(5);

        horizontalLayout_7->addWidget(spinBox_6);

        spinBox_4 = new QSpinBox(groupBox_8);
        spinBox_4->setObjectName(QString::fromUtf8("spinBox_4"));
        spinBox_4->setMinimum(-5);
        spinBox_4->setMaximum(5);

        horizontalLayout_7->addWidget(spinBox_4);


        verticalLayout_3->addWidget(groupBox_8);


        verticalLayout->addWidget(groupBox_5);

        groupBox_11 = new QGroupBox(groupBox);
        groupBox_11->setObjectName(QString::fromUtf8("groupBox_11"));
        verticalLayout_4 = new QVBoxLayout(groupBox_11);
        verticalLayout_4->setSpacing(6);
        verticalLayout_4->setContentsMargins(11, 11, 11, 11);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        groupBox_12 = new QGroupBox(groupBox_11);
        groupBox_12->setObjectName(QString::fromUtf8("groupBox_12"));
        horizontalLayout_10 = new QHBoxLayout(groupBox_12);
        horizontalLayout_10->setSpacing(6);
        horizontalLayout_10->setContentsMargins(11, 11, 11, 11);
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        spinBox_10 = new QSpinBox(groupBox_12);
        spinBox_10->setObjectName(QString::fromUtf8("spinBox_10"));
        spinBox_10->setMaximum(300);
        spinBox_10->setValue(100);

        horizontalLayout_10->addWidget(spinBox_10);

        spinBox_9 = new QSpinBox(groupBox_12);
        spinBox_9->setObjectName(QString::fromUtf8("spinBox_9"));
        spinBox_9->setMaximum(300);
        spinBox_9->setValue(100);

        horizontalLayout_10->addWidget(spinBox_9);

        spinBox_8 = new QSpinBox(groupBox_12);
        spinBox_8->setObjectName(QString::fromUtf8("spinBox_8"));
        spinBox_8->setMaximum(300);
        spinBox_8->setValue(100);

        horizontalLayout_10->addWidget(spinBox_8);


        verticalLayout_4->addWidget(groupBox_12);


        verticalLayout->addWidget(groupBox_11);

        pushButton_2 = new QPushButton(groupBox);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        verticalLayout->addWidget(pushButton_2);

        playRX_10 = new QPushButton(groupBox);
        playRX_10->setObjectName(QString::fromUtf8("playRX_10"));

        verticalLayout->addWidget(playRX_10);

        pushButton = new QPushButton(groupBox);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        verticalLayout->addWidget(pushButton);


        horizontalLayout->addWidget(groupBox);

        JanelaPrincipal->setCentralWidget(centralWidget);
        groupBox->raise();
        widget->raise();
        menuBar = new QMenuBar(JanelaPrincipal);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 848, 17));
        menu3D = new QMenu(menuBar);
        menu3D->setObjectName(QString::fromUtf8("menu3D"));
        JanelaPrincipal->setMenuBar(menuBar);
        mainToolBar = new QToolBar(JanelaPrincipal);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        JanelaPrincipal->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(JanelaPrincipal);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        JanelaPrincipal->setStatusBar(statusBar);

        menuBar->addAction(menu3D->menuAction());

        retranslateUi(JanelaPrincipal);
        QObject::connect(pushButton, SIGNAL(clicked()), JanelaPrincipal, SLOT(close()));
        QObject::connect(spinBox, SIGNAL(valueChanged(int)), widget, SLOT(setRotacaoX(int)));
        QObject::connect(spinBox_3, SIGNAL(valueChanged(int)), widget, SLOT(setRotacaoZ(int)));
        QObject::connect(spinBox_2, SIGNAL(valueChanged(int)), widget, SLOT(setRotacaoY(int)));
        QObject::connect(playRX_10, SIGNAL(clicked()), widget, SLOT(animateObj()));
        QObject::connect(playRX_10, SIGNAL(clicked()), widget, SLOT(ativarScale()));
        QObject::connect(spinBox_10, SIGNAL(valueChanged(int)), widget, SLOT(setEscalaX(int)));
        QObject::connect(spinBox_9, SIGNAL(valueChanged(int)), widget, SLOT(setEscalaY(int)));
        QObject::connect(spinBox_8, SIGNAL(valueChanged(int)), widget, SLOT(setEscalaZ(int)));
        QObject::connect(pushButton_2, SIGNAL(clicked()), widget, SLOT(resetModel()));
        QObject::connect(spinBox_4, SIGNAL(valueChanged(int)), widget, SLOT(setTranslacaoZ(int)));
        QObject::connect(spinBox_6, SIGNAL(valueChanged(int)), widget, SLOT(setTranslacaoY(int)));
        QObject::connect(spinBox_7, SIGNAL(valueChanged(int)), widget, SLOT(setTranslacaoX(int)));

        QMetaObject::connectSlotsByName(JanelaPrincipal);
    } // setupUi

    void retranslateUi(QMainWindow *JanelaPrincipal)
    {
        JanelaPrincipal->setWindowTitle(QApplication::translate("JanelaPrincipal", "JanelaPrincipal", 0, QApplication::UnicodeUTF8));
        groupBox->setTitle(QApplication::translate("JanelaPrincipal", "Menu", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("JanelaPrincipal", "Rota\303\247\303\243o", 0, QApplication::UnicodeUTF8));
        groupBox_7->setTitle(QApplication::translate("JanelaPrincipal", "Rota\303\247\303\243o no X", 0, QApplication::UnicodeUTF8));
        groupBox_4->setTitle(QApplication::translate("JanelaPrincipal", "Rota\303\247\303\243o no Z", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QApplication::translate("JanelaPrincipal", "Rota\303\247\303\243o no Y", 0, QApplication::UnicodeUTF8));
        groupBox_5->setTitle(QApplication::translate("JanelaPrincipal", "Transla\303\247\303\243o", 0, QApplication::UnicodeUTF8));
        groupBox_8->setTitle(QApplication::translate("JanelaPrincipal", "X            Y             Z", 0, QApplication::UnicodeUTF8));
        groupBox_11->setTitle(QApplication::translate("JanelaPrincipal", "Escala(%)", 0, QApplication::UnicodeUTF8));
        groupBox_12->setTitle(QApplication::translate("JanelaPrincipal", "X            Y             Z", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("JanelaPrincipal", "Resetar", 0, QApplication::UnicodeUTF8));
        playRX_10->setText(QApplication::translate("JanelaPrincipal", "Play", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("JanelaPrincipal", "Sair", 0, QApplication::UnicodeUTF8));
        menu3D->setTitle(QApplication::translate("JanelaPrincipal", "3D", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class JanelaPrincipal: public Ui_JanelaPrincipal {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_JANELAPRINCIPAL_H
